﻿using DGJW.Dal.SysMange;
using DGJW.Model;
using DGJW.Web.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGJW.Web.Controllers
{
    public class FilesController : Controller
    {
        public ActionResult Index()
        {
            GDJWContext db = new GDJWContext();
            ViewBag.list = db.FilesSet.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult List()
        {
            GDJWContext db = new GDJWContext();
            ViewData.Model = db.FilesSet.ToList();
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public void Create(FilesSet files, HttpPostedFileBase file)
        {
            var fileName = file.FileName;
            var filePath = Server.MapPath("~/File");
            file.SaveAs(Path.Combine(filePath, fileName));

            files.Path = fileName;
            GDJWContext db = new GDJWContext();
            db.FilesSet.Add(files);
            db.SaveChanges();
            db.Dispose();
            Response.Redirect("/Files/List");
        }

        public void Delete(int id)
        {
            GDJWContext db = new GDJWContext();
            var f =db.FilesSet.Find(id);
            if (f!=null)
            {
                db.FilesSet.Remove(f);
                db.SaveChanges();
            }
            Response.Redirect("/Files/List");
        }

        public ActionResult Edit(int id)
        {
            GDJWContext db = new GDJWContext();
            var f = db.FilesSet.Find(id);
            ViewData.Model = f;
            return View();
        }

        [HttpPost]
        public void Edit(FilesSet filesSet)
        {
            GDJWContext db = new GDJWContext();
            db.FilesSet.Update(filesSet);
            db.SaveChanges();
            Response.Redirect("/Files/List");
        }
    }
}