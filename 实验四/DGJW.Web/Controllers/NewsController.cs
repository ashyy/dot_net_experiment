﻿using DGJW.Bll.NewsManage;
using DGJW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGJW.Web.Controllers
{
    public class NewsController : Controller
    {
        NewsBll bll = new NewsBll();
        // GET: News
        public ActionResult Index()
        {
            List<News> newsList = bll.Query().ToList();
            ViewBag.newsList = newsList;
            return View();
        }

        public ActionResult News()
        {
            List<News> newsList = bll.Query().ToList();
            ViewBag.newsList = newsList;
            ViewBag.act = 2;
            return View();
        }

        // GET: Create
        public ActionResult Create()
        {
            ViewBag.act = 1;
            return View();
        }

        [HttpPost]
        public void Create(News news)
        {
            news.Code = "067677";
            news.CreateTime = DateTime.Now;
            news.Hits = 0;
            news.IsRec = 0;
            bll.Add(news);
            Response.Redirect("/News/News");
        }

       
        public void ExSuc(Int64 id)
        {

            var news = bll.Find(id);
            if (news!=null)
            {
                news.IsRec = 2;
            }
            bll.Update(news);
            Response.Redirect("/News/News");
        }

        
        public void ExFal(Int64 id)
        {

            var news = bll.Find(id);
            if (news != null)
            {
                news.IsRec = 1;
            }
            bll.Update(news);
            Response.Redirect("/News/News");
        }

        
        public void ExRjt(Int64 id)
        {

            var news = bll.Find(id);
            if (news != null)
            {
                news.IsRec = 3;
            }
            bll.Update(news);
            Response.Redirect("/News/News");
        }

        // GET: Create
        public void Delete(Int64 Id)
        {

            bll.Delete(Id);

            Response.Redirect("/News/News");

        }

        public ActionResult Update(int Id)
        {
            News news = bll.Find(Id);
            ViewBag.news = news;
            return View();
        }

        [HttpPost]
        public void Update(News news)
        {
            bll.Update(news);
            Response.Redirect("/News/News");

        }
    }
}