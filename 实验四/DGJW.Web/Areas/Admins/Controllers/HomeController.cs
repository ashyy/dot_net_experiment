﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGJW.Web.Areas.Admins.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admins/Home
        public ActionResult Index()
        {
            ViewBag.act = 0;
            return View();
        }
    }
}