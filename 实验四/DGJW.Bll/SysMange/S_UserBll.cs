﻿
using DGJW.Bll.Base;
using DGJW.Dal.SysMange;
using DGJW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Bll.SysMange
{
    public class S_UserBll : BaseBll<S_User>
    {
        private S_UserDal dal = new S_UserDal();
        public override void SetDal()
        {
            baseDal = dal;
        }

        public S_User GetUserLogin(S_User query)
        {
            S_User user = dal.Query(o => o.Account==query.Account&&o.Password==query.Password).FirstOrDefault();
            return user;
        }

        public S_User GetUser(S_User query)
        {
            S_User user = dal.Query(o => o.Account == query.Account).FirstOrDefault();
            return user;
        }

        public S_User RegisterUser(S_User query)
        {
            S_User user = dal.Query(o => o.Account == query.Account).FirstOrDefault();
            if (user==null)
            {
                query.CheckStatus = 1;
                var idcard = Guid.NewGuid().ToString();
                query.IdCard = idcard.Substring(idcard.Length-20);
                dal.Add(query);
                dal.SaveChanges();
                user = query;
            }
            return user;
        }

    }
}
