﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Bll.Base
{
    public abstract partial class BaseBll<T> where T : class, new()
    {
        protected DGJW.Dal.Base.BaseDal<T> baseDal; //没有用依赖注入
        public BaseBll()
        {
            SetDal();
        }

        public abstract void SetDal();

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="t">实体</param>   
        virtual public bool Add(T t)
        {
            return baseDal.Add(t);
        }

        /// <summary>
        /// 异步添加
        /// </summary>
        /// <param name="t">实体</param>      
        virtual public async Task<bool> AddAsync(T t)
        {
            return await baseDal.AddAsync(t);
        }

        /// <summary>
        /// 添加[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>   
        virtual public bool AddRange(IEnumerable<T> ts)
        {
            return baseDal.AddRange(ts);
        }

        /// <summary>
        /// 异步添加[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>     
        virtual public async Task<bool> AddRangeAsync(IEnumerable<T> ts)
        {
            return await baseDal.AddRangeAsync(ts);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="t">实体</param>
        virtual public bool Delete(T t)
        {
            return baseDal.Delete(t);
        }

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="t">实体</param>
        virtual public async Task<bool> DeleteAsync(T t)
        {
            return await baseDal.DeleteAsync(t);
        }

        /// <summary>
        /// 删除[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>
        virtual public bool DeleteRange(IEnumerable<T> ts)
        {
            return baseDal.DeleteRange(ts);
        }

        /// <summary>
        /// 异步删除[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>
        virtual public async Task<bool> DeleteRangeAsync(IEnumerable<T> ts)
        {
            return await baseDal.DeleteRangeAsync(ts);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="t">实体</param>
        virtual public bool Update(T t)
        {
            return baseDal.Update(t);
        }

        /// <summary>
        /// 异步更新
        /// </summary>
        /// <param name="t">实体</param>
        virtual public async Task<bool> UpdateAsync(T t)
        {
            return await baseDal.UpdateAsync(t);
        }

        /// <summary>  
        /// 确定序列的任何元素是否满足条件 
        /// </summary>  
        /// <param name="anyLambda"></param>        
        virtual public bool Exists(Expression<Func<T, bool>> anyLambda)
        {
            return baseDal.Exists(anyLambda);
        }

        /// <summary>  
        ///  异步确定序列的任何元素是否满足条件
        /// </summary>  
        /// <param name="anyLambda"></param>       
        virtual public async Task<bool> ExistsAsync(Expression<Func<T, Boolean>> anyLambda)
        {
            return await baseDal.ExistsAsync(anyLambda);
        }

        /// <summary>  
        /// 根据主键获取实体,不存在返回Null  
        /// </summary>  
        /// <param name="key"></param>  
        /// <returns name="T">实体</returns>  
        virtual public T Find(object key)
        {
            return baseDal.Find(key);
        }

        /// <summary>  
        /// 异步根据主键获取实体,不存在返回Null  
        /// </summary>  
        /// <param name="key"></param>  
        /// <returns name="T">实体</returns>   
        virtual public async Task<T> FindAsync(object key)
        {
            return await baseDal.FindAsync(key);
        }

        /// <summary>  
        /// 根据查询条件返回序列的第一个元素,不存在返回Null    
        /// </summary>  
        /// <param name="whereLambda">查询表达式</param>  
        /// <returns></returns>  
        virtual public T Find(Expression<Func<T, bool>> whereLambda)
        {
            return baseDal.Find(whereLambda);
        }


        /// <summary>  
        /// 异步根据查询条件返回序列的第一个元素,不存在返回Null   
        /// </summary>  
        /// <param name="whereLambda">查询表达式</param>  
        /// <returns name="T">实体</returns>  
        virtual public async Task<T> FindAsync(Expression<Func<T, bool>> whereLambda)
        {
            return await baseDal.FindAsync(whereLambda);
        }

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns>IQueryable集合</returns>
        virtual public IQueryable<T> Query(Expression<Func<T, bool>> whereLambda)
        {
            return baseDal.Query(whereLambda);
        }

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns>IQueryable集合</returns>
        virtual public IQueryable<T> Query()
        {
            return baseDal.Query();
        }

    

        virtual public bool SaveChanges()
        {
            return baseDal.SaveChanges();
        }
    }
}
