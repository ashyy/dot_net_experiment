﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DGJW.Web.Entity
{
    public partial class GDJWContext : DbContext
    {
        public GDJWContext()
        {
        }

        public GDJWContext(DbContextOptions<GDJWContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FilesSet> FilesSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=GDJW;uid=sa;pwd=sa");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FilesSet>(entity =>
            {
                entity.Property(e => e.Describe).IsRequired();

                entity.Property(e => e.FileName).IsRequired();

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("path");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");
            });


            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
