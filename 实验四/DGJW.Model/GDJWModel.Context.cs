﻿

//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------


namespace DGJW.Model
{

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


public partial class GDJWEntities : DbContext
{
    public GDJWEntities()
        : base("name=GDJWEntities")
    {

    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }


    public virtual DbSet<News> News { get; set; }

    public virtual DbSet<S_User> S_User { get; set; }

    public virtual DbSet<S_Module> S_Module { get; set; }

    public virtual DbSet<Files> FilesSet { get; set; }

}

}

