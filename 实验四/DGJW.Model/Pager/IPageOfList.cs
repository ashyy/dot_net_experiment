﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace DGJW.Model.Pager {
	public interface IPageOfList {
		long CurrentStart { get; }
		int PageIndex { get; set; }
		int PageSize { get; set; }
		int PageTotal { get; }
		long RecordTotal { get; set; }
	}

	public interface IPageOfList<T> : IPageOfList, IList<T> {

	}
	public class PageOfList<T> : List<T>, IList<T>, IPageOfList, IPageOfList<T> {

		public PageOfList(IEnumerable<T> items, int pageIndex, int pageSize, long recordTotal) {
			if (items != null)
				AddRange(items);
			PageIndex = pageIndex;
			PageSize = pageSize;
			RecordTotal = recordTotal;
		}

		public PageOfList(int pageSize) {
			if (pageSize <= 0) {
				throw new ArgumentException("页面数据量必须大于0", "页面数据量");
			}
		}
		public int PageIndex { get; set; }
		public int PageSize { get; set; }

		public int PageTotal {
			get {
				//RecordTotal / PageSize  获取能够被布满的页面数，(RecordTotal % PageSize > 0 ? 1 : 0）判断是否有未布满的页面。
				return (int)RecordTotal / PageSize + (RecordTotal % PageSize > 0 ? 1 : 0);
			}
		}

		public long RecordTotal { get; set; }
		/// <summary>
		/// 当前页面的记录开始位置
		/// </summary>
		public long CurrentStart {
			get { return PageIndex * PageSize + 1; }
		}
		/// <summary>
		/// 当前页面的结束位置
		/// </summary>
		public long CurrentEnd {
			get { return (PageIndex + 1) * PageSize > RecordTotal ? RecordTotal : (PageIndex + 1) * PageSize; }
		}
	}
}