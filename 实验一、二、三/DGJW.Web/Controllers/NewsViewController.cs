﻿using DGJW.Bll.NewsManage;
using DGJW.Model;
using DGJW.Model.Pager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DGJW.Web.Controllers
{
    public class NewsViewController :Controller
    {
        NewsBll bll = new NewsBll();
        // GET: News
        public ActionResult Index()
        {
            List<News> newsList = bll.Query().ToList();
            ViewBag.newsList = newsList;
            return View();
        }

        public ActionResult IndexByPage(int index)
        {
            List<News> newsList = bll.Query().ToList();
            newsList = newsList.Skip(3 * index).Take(3).ToList();
            var count = bll.Query().Count();
            PageOfList<News> newsPageList = new PageOfList<News>(newsList, index, 3, count);
            ViewBag.newsPageList = newsPageList;
            ViewBag.key = null;
            return View("~/Views/NewsView/_NewsListPart.cshtml");
        }


        public ActionResult IndexBySearch(string key,int index)
        {
            var list = bll.Query().Where((n)=>n.Keyword.Contains(key)||n.Title.Contains(key));
            List<News> newsList = new List<News>();
            if (list!=null)
            {
                newsList = list.ToList();
            }
            var count = newsList.Count();
            newsList = newsList.Skip(3 * index).Take(3).ToList();
            PageOfList<News> newsPageList = new PageOfList<News>(newsList,index, 3, count);
            ViewBag.newsPageList = newsPageList;
            ViewBag.key = key;
            return View("~/Views/NewsView/_NewsListPart.cshtml");
        }

        public ActionResult New(Int64 Id)
        {
            News news = bll.Find(Id);
            ViewBag.news = news;
            news.Hits++;
            bll.SaveChanges();
            return View();
        }

    }
}