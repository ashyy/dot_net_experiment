﻿using DGJW.Bll.SysMange;
using DGJW.Dal.SysMange;
using DGJW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using Utility.Security;

namespace DGJW.Web.Areas.Admins.Controllers
{
    public class RegisterController : Controller
    {
        S_UserBll bll = new S_UserBll();

        // GET: Admins/Register
        public ActionResult Index()
        {
            return View();
        }



        #region 验证码

        /// <summary>
        /// 生成验证码图像
        /// </summary>
        /// <returns>验证码图像</returns>
        public FileResult GetVCodeImg()
        {
            ValidateCode validateCode = new ValidateCode();
            string code = validateCode.CreateValidateCode(4);
            Session["VCode"] = code;
            byte[] bytes = validateCode.CreateValidateGraphic(code);
            return File(bytes, @"image/gif");
        }

        /// <summary>
        /// 获取验证码
        /// </suummary>
        /// <returns>验证码</returns>
        public string GetVCode()
        {
            return Session["VCode"].ToString();
        }

        #endregion


        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="query">用户名、密码、验证码</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(S_User query, string VCode)
        {
            if (string.IsNullOrEmpty(query.Account))
            {
                return Json(new { code = "-1", msg = "请输入用户名!" }, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(query.Password))
            {
                return Json(new { code = "-1", msg = "请输入密码!" }, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(query.Name))
            {
                return Json(new { code = "-1", msg = "请输入用户名!" }, JsonRequestBehavior.AllowGet);
            }

            string sCode_op = VCode.Trim();

            if (VCode.Trim() != GetVCode())
            {
                return Json(new { code = "-1", msg = "验证码错误!" }, JsonRequestBehavior.AllowGet);
            }
            query.Password = Utility.Security.Encrypt.BuildPassword(query.Password.Trim());   //密码加密
            S_User user = bll.GetUser(query);
            if (user != null)
            {
                return Json(new { code = "-1", msg = "用户名已存在!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                user = bll.RegisterUser(query);
                if (user.CheckStatus != 1)
                {
                    return Json(new { code = "-1", msg = "您的账号未审核通过!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Session["User"] = user;
                    //MyLog.LogAdd(user.Name, "用户", "登录日志", "", "登录成功");
                    return Json(new { code = "-1", msg = "注册成功", url = "/Admins/Auth/Index" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}