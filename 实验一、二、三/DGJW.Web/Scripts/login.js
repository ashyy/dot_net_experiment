﻿if (top != self) {
    top.location.href = "Index";
}

$(document).ready(function () {
    $("#Account").focus();
});

function login() {
    $("#loginform").find("li.error").remove();
    $.ajax({
        url: "/Auth/Login",
        dataType: 'json',
        type: 'POST',
        data: "Account=" + $("#Account").val() + "&Password=" + $("#Password").val() + "&VCode=" + $("#VCode").val(),
        success: function (data, textStatus, XMLHttpRequest) {
            if (data.code == 1) {
                document.location.href = data.url;
            }
            else if (data.code == -1) {
                $("#error").html(data.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function register() {
    $("#loginform").find("li.error").remove();
    $.ajax({
        url: "/Admins/Register/Register",
        dataType: 'json',
        type: 'POST',
        data: "Account=" + $("#Account").val() + "&Password=" + $("#Password").val() + "&Name=" + $("#Name").val()+ "&VCode=" + $("#VCode").val(),
        success: function (data, textStatus, XMLHttpRequest) {
            if (data.code == 1) {
                document.location.href = data.url;
            }
            else if (data.code == -1) {
                $("#error").html(data.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

document.onkeydown = function () {
    if (event.keyCode == 13) {
        $("#btSubmit").click();
    }
}

function GetVCodeImg() {
    $("#imgVCode").attr("src", "/Auth/GetVCodeImg?t=" + new Date().getSeconds());
}