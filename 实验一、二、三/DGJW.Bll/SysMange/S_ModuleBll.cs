﻿using DGJW.Bll.Base;
using DGJW.Dal.SysMange;
using DGJW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Bll.SysMange
{
    public class S_ModuleBll : BaseBll<S_Module>
    {
        private S_ModuleDal dal = new S_ModuleDal();
        public override void SetDal()
        {
            baseDal = dal;
        }
    }
}
