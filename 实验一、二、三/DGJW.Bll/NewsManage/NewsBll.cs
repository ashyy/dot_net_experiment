﻿using DGJW.Bll.Base;
using DGJW.Dal.NewsManage;
using DGJW.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Bll.NewsManage
{
    public class NewsBll : BaseBll<News>
    {
        private NewsDal dal = new NewsDal();
        public override void SetDal()
        {
            baseDal = dal;
        }

        /// <summary>
        /// 异步删除-根据主表ID，主从表一起删除
        /// </summary>
        /// <param name="t">实体</param>
        public  bool Delete(Int64 Id)
        {
            News news = Find(Id);
            return Delete(news);
        }

    }
}
