﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Model
{
    public partial class S_User
    {
        public List<S_Module> Modules{ get; set; }
        public Int64 CurrentModuleId { get; set; }
    }
}
