﻿using System;
using System.Collections.Generic;

namespace DGJW.Web.Entity
{
    public partial class FilesSet
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Describe { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
    }
}
