
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/08/2020 19:46:01
-- Generated from EDMX file: C:\Users\a9457\Desktop\GDJW第二阶段1-登陆\实验一\DGJW.Model\GDJWModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GDJW];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[News]', 'U') IS NOT NULL
    DROP TABLE [dbo].[News];
GO
IF OBJECT_ID(N'[dbo].[S_User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[S_User];
GO
IF OBJECT_ID(N'[dbo].[S_Module]', 'U') IS NOT NULL
    DROP TABLE [dbo].[S_Module];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'News'
CREATE TABLE [dbo].[News] (
    [Id] bigint identity(1,1)   NOT NULL,
    [Title] varchar(200)  NOT NULL,
    [Author] varchar(20)  NULL,
    [Source] varchar(50)  NULL,
    [Keyword] varchar(50)  NULL,
    [Introduce] varchar(800)  NULL,
    [Contents] nvarchar(max)  NULL,
    [CreateTime] datetime  NULL,
    [CreatUserName] varchar(20)  NULL,
    [Hits] int  NULL,
    [IsRec] int  NULL,
    [IsTop] int  NULL,
    [IsHead] int  NULL,
    [CheckStatus] int  NULL,
    [CheckUserName] varchar(20)  NULL,
    [CheckTime] datetime  NULL,
    [CheckMeno] varchar(200)  NULL,
    [Type] bigint  NULL,
    [Code] varchar(20)  NULL,
    [Link] varchar(200)  NULL
);
GO

-- Creating table 'S_User'
CREATE TABLE [dbo].[S_User] (
    [ID] bigint identity(1,1)   NOT NULL,
    [Account] varchar(20)  NOT NULL,
    [Password] varchar(20)  NOT NULL,
    [Name] varchar(20)  NULL,
    [Sex] int  NULL,
    [Tel] varchar(20)  NULL,
    [Email] varchar(50)  NULL,
    [Status] int  NULL,
    [IdCard] varchar(20)  NOT NULL,
    [PhotoUrl] varchar(100)  NULL,
    [CreateTime] datetime  NULL,
    [CreatUserName] varchar(20)  NULL,
    [DepartMentID] bigint  NULL,
    [Meno] varchar(200)  NULL,
    [CheckStatus] int  NULL,
    [CheckUserName] varchar(20)  NULL,
    [CheckTime] datetime  NULL,
    [CheckMeno] varchar(200)  NULL,
    [Code] varchar(20)  NULL,
    [DataLevel] int  NULL
);
GO

-- Creating table 'S_Module'
CREATE TABLE [dbo].[S_Module] (
    [ID] bigint  NOT NULL,
    [Code] varchar(20)  NOT NULL,
    [Name] varchar(50)  NOT NULL,
    [Oders] int  NULL,
    [IsLeaf] int  NULL,
    [IsMenu] int  NULL,
    [Url] varchar(200)  NULL,
    [AdminUrl] varchar(200)  NULL,
    [IsSystem] int  NULL,
    [Icon] varchar(200)  NULL,
    [Meno] varchar(500)  NULL,
    [Target] varchar(20)  NULL,
    [IconFront] varchar(200)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'News'
ALTER TABLE [dbo].[News]
ADD CONSTRAINT [PK_News]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'S_User'
ALTER TABLE [dbo].[S_User]
ADD CONSTRAINT [PK_S_User]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'S_Module'
ALTER TABLE [dbo].[S_Module]
ADD CONSTRAINT [PK_S_Module]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------