﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DGJW.Dal.Base
{
    /// <summary>
    /// Dal层访问基类
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class BaseDal<T> where T : class, new()
    {
        protected DbContext dbContext = DbContextFactory.Create();

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="t">实体</param>      
        virtual public bool Add(T t)
        {
            dbContext.Set<T>().Add(t);
            return dbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 异步添加
        /// </summary>
        /// <param name="t">实体</param>      
        virtual public async Task<bool> AddAsync(T t)
        {
            dbContext.Set<T>().Add(t);
            return await dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 添加[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>    
        virtual public bool AddRange(IEnumerable<T> ts)
        {
            dbContext.Set<T>().AddRange(ts);
            return dbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 异步添加[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>     
        virtual public async Task<bool> AddRangeAsync(IEnumerable<T> ts)
        {
            dbContext.Set<T>().AddRange(ts);
            return await dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="t">实体</param>
        virtual public bool Delete(T t)
        {
            dbContext.Set<T>().Remove(t);
            return dbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 异步删除
        /// </summary>
        /// <param name="t">实体</param>
        virtual public async Task<bool> DeleteAsync(T t)
        {
            dbContext.Set<T>().Remove(t);
            return await dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 删除[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>
        virtual public bool DeleteRange(IEnumerable<T> ts)
        {
            dbContext.Set<T>().RemoveRange(ts);
            return dbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 异步删除[批量]
        /// </summary>
        /// <param name="ts">IEnumerable实体集</param>
        virtual public async Task<bool> DeleteRangeAsync(IEnumerable<T> ts)
        {
            dbContext.Set<T>().RemoveRange(ts);
            return await dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="t">实体</param>
        virtual public bool Update(T t)
        {
            dbContext.Set<T>().AddOrUpdate(t);
            return dbContext.SaveChanges() > 0;
        }

        /// <summary>
        /// 异步更新
        /// </summary>
        /// <param name="t">实体</param>
        virtual public async Task<bool> UpdateAsync(T t)
        {
            dbContext.Set<T>().AddOrUpdate(t);
            return await dbContext.SaveChangesAsync() > 0;
        }

        /// <summary>  
        /// 确定序列的任何实体是否满足条件 
        /// </summary>  
        /// <param name="anyLambda"></param>        
        virtual public bool Exists(Expression<Func<T, bool>> anyLambda)
        {
            return dbContext.Set<T>().Any(anyLambda);
        }

        /// <summary>  
        ///  异步确定序列的任何实体是否满足条件
        /// </summary>  
        /// <param name="anyLambda"></param>       
        virtual public async Task<bool> ExistsAsync(Expression<Func<T, Boolean>> anyLambda)
        {
            return await dbContext.Set<T>().AnyAsync(anyLambda);
        }

        /// <summary>  
        /// 根据主键获取实体  如果上下文中存在带给定主键值的实体，则立即返回该实体，而不会向存储区发送请求。 否则，会向存储区发送查找带给定主键值的实体的请求，如果找到该实体，则将其附加到上下文并返回。 如果未在上下文或存储区中找到实体，则返回 null。  
        /// </summary>  
        /// <param name="key"></param>  
        /// <returns></returns>  
        virtual public T Find(object key)
        {
            return dbContext.Set<T>().Find(key);
        }

        /// <summary>  
        /// 异步根据主键获取实体  如果上下文中存在带给定主键值的实体，则立即返回该实体，而不会向存储区发送请求。 否则，会向存储区发送查找带给定主键值的实体的请求，如果找到该实体，则将其附加到上下文并返回。 如果未在上下文或存储区中找到实体，则返回 null。  
        /// </summary>  
        /// <param name="key"></param>  
        /// <returns></returns>  
        virtual public async Task<T> FindAsync(object key)
        {
            return await dbContext.Set<T>().FindAsync(key);
        }

        /// <summary>  
        /// 根据查询条件返回序列的第一个元素  
        /// </summary>  
        /// <param name="whereLambda">查询表达式</param>  
        /// <returns></returns>  
        virtual public T Find(Expression<Func<T, bool>> whereLambda)
        {
            return dbContext.Set<T>().FirstOrDefault<T>(whereLambda);
        }


        /// <summary>  
        /// 异步根据查询条件返回序列的第一个元素 
        /// </summary>  
        /// <param name="whereLambda">查询表达式</param>  
        /// <returns></returns>  
        virtual public async Task<T> FindAsync(Expression<Func<T, bool>> whereLambda)
        {
            return await dbContext.Set<T>().FirstOrDefaultAsync<T>(whereLambda);
        }

        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <returns>IQueryable集合</returns>
        virtual public IQueryable<T> Query()
        {
            return dbContext.Set<T>();
        }

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <param name="whereLambda">查询表达式</param>
        /// <returns>IQueryable集合</returns>
        virtual public IQueryable<T> Query(Expression<Func<T, bool>> whereLambda)
        {
            return dbContext.Set<T>().Where(whereLambda);
        }

        /// <summary>
        /// 查询数据列表
        /// </summary>
        /// <typeparam name="type"></typeparam>
        /// <param name="whereLambda">查询表达式</param>
        /// <param name="OrderByLambda">排序表达式</param>
        /// <param name="isAsc">是否升序</param>
        /// <returns>IQueryable集合</returns>
        virtual public IQueryable<T> Query<type>(Expression<Func<T, bool>> WhereLambda, Expression<Func<T, type>> OrderByLambda, bool isAsc)
        {
            var temp = dbContext.Set<T>().AsQueryable(); ;
            if (WhereLambda != null)
                temp = temp.Where(WhereLambda);
            if (OrderByLambda != null)
            {
                if (isAsc)  //是否升序
                    temp = temp.OrderBy(OrderByLambda);
                else
                    temp = temp.OrderByDescending(OrderByLambda);
            }
            return temp;
        }

        /// <summary>
        ///  保存更改
        /// </summary>
        /// <returns></returns>
        virtual public bool SaveChanges()
        {
            return dbContext.SaveChanges() > 0;
        }
    }
}
